        /* 
        Функция увеличения здоровья, 2 hp/сек
        */
        var hp;
        var ep;
        var player;
        
        function inc_hp(){
            hp = document.getElementById('hp');
            hp.innerText;
            if (hp.innerText <= 98) {
                hp.innerText = Number(hp.innerText)+2;
            }
            end();
        }
        setInterval(inc_hp,1000);

        function end(){
            if (hp.innerText <= 0) {
                window.location.href = 'index.html';
            }
        }

        /* 
            Функция увеличения энергии, 5 ep/сек
        */
       
        function inc_ep(){
            ep = document.getElementById('ep');
            ep.innerText;
            if (ep.innerText <= 95) {
                ep.innerText = Number(ep.innerText)+5;
            }
        }
        setInterval(inc_ep,1000);

        var player = document.getElementById('knight');
        player.style.background = "url(image/idle.gif) no-repeat";
        player.style.backgroundSize = "300px 300px";
        player.style.transform = 'scale(1, 1)';


        function stay() {
            var lpos = move.lpos;   
            window.flag = false;
            
            player.style.background = "url(image/idle.gif) no-repeat";
            player.style.backgroundSize = "300px 300px";
            player.style.left = lpos + 'px'; 
        }
    

        function move (event) {
            var view = 1;
            var lpos = move.lpos;   

            switch(event.keyCode) {
                case 37:
                    player.style.transform = 'scale(-1, 1)';
                    player.style.background = "url(image/walk.gif) no-repeat";
                    player.style.backgroundSize = "330px 300px";
                    window.view = 0;
                    if(lpos > 0){
                        lpos= lpos - 10;
                        player.style.left = lpos + 'px';  
                    }
                    break;
                case 39: 
                    player.style.transform = 'scale(1, 1)';
                    player.style.background = "url(image/walk.gif) no-repeat";  
                    player.style.backgroundSize = "300px 300px";
                    window.view = 1;
                    if(lpos < 1000){
                        lpos= lpos + 10;
                        player.style.left = lpos + 'px'; 
                    }
                }
            addEventListener("keyup", stay);
            move.lpos = lpos; 
        }
        move.lpos = 0;
        addEventListener("keydown", move);


        var flag = false;
        function fight(event){
            if (event.keyCode == 97 || event.keyCode == 49) {
                if (ep.innerText >= 10) {
                    if (!window.flag){
                        ep.innerText = Number(ep.innerText) - 10;
                        window.flag = true;
                    }
                    player.style.background = "url(image/attack1_loop.gif) no-repeat ";
                    player.style.backgroundSize = "300px 300px";
                }
            }

            if (event.keyCode == 98 || event.keyCode == 50) {
                if (ep.innerText >= 15) {
                   if (!window.flag){
                        ep.innerText = Number(ep.innerText) - 15;
                       window.flag = true;
                   }
                    player.style.background = "url(image/block.gif) no-repeat";
                    player.style.backgroundSize = "300px 300px";
                }
            }

            if (event.keyCode == 99 || event.keyCode == 51) {
                if (ep.innerText >= 30 ) {
                    if (!window.flag){
                        ep.innerText = Number(ep.innerText) - 30;
                        window.flag = true;   
                    }
                    player.style.background = "url(image/attack2_loop.gif) no-repeat";
                    player.style.backgroundSize = "300px 300px";
                }
            }
        }
        addEventListener("keyup", stay);

        window.flag = false;
        addEventListener("keydown", fight); 


       