    /*
    Организует таймер в виде 00:00(mm:ss) на экране
    */

    window.onload = function(){ 
        function timer(){
            var minute = document.getElementById('minute').innerHTML;
            var second = document.getElementById('second').innerHTML;
            
            if( second < 59 ) {
                second++;
            } else{
                second = 0;
                minute++;
            }
            document.getElementById('minute').innerHTML = minute;
            document.getElementById('second').innerHTML = second;
            
        }
        window.intervalID = setInterval(timer, 1000);
    }